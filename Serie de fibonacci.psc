Algoritmo sin_titulo
	//Sucesi�n Fibonacci se define de la siguiente forma a(1)=1, a(2)=1 y a(n)=a(n-1)+a(n-2)
	//para n>2, es decir los dos primeros son 1 y el resto de cada uno es la suma de los anteriores,
	//los primeros n�meros de la sicesi�n son: 1,1,2,3,5,8,13,21... hace un diagrama de flujo 
	//para calcular el n-esimo termino de la sucesi�n
	
	a=0
	b=1
	c=0
	Leer n
	Mientras c<n-1 Hacer
		Escribir d
		c=c+1
		d=a+b
		a=b
		b=d
	Fin Mientras
FinAlgoritmo
